## Keebie Gui
### A project to map any keyboard key to commands with a user friendly interface (on linux).
Eventually, it will allow to easily create layer files for [Keebie](https://github.com/robinuniverse/Keebie), even for someone who is not familiar with the command line.
![Screenshot](resources/screenshot.png)

### Current status :
This project is already usable but still a work in progress, made for my personal purposes. It is lacking some major features.

### Roadmap :
- Currently :   
creating plugins that are necessary for my own workflow.

- Near future (end of August at worst hopefully) :  
adding basic missing features (ex : delete layers, change keebie directory, save as option, leds support...)  
promoting  
Possibility to add icons to each keymap. Then create an exporter to make it an whole image, making easy to print it and label the keyboard.

- In the long run :   
there could be an online library of plugins to choose from.  
adding more additional features

### How to use :
There should be a linux x11 build under the release tab. Download and extract it. From here it is pretty straight forward. Keebie is packed with the build. Configure it by following the setup instructions on [Keebie repo](https://github.com/robinuniverse/Keebie). You may want to add keebie.py to your startup programs so it is enabled as soon as you boot your computer.

### Building :
Install [Godot](https://godotengine.org/), open project.godot and export it (follow [this guide](https://docs.godotengine.org/en/stable/getting_started/workflow/export/exporting_projects.html) if you need help). Don't forget to add a Keebie install at the root directory of the project.

### Contributing :
If you are interested in this project, please give a star.
The best way to contribute to this project is to create plugins for software you would like to see supported. You just need some basic json and command line knowledge.  
If you are interested, wait for the documentation be written.  
You can also open pull requests if you want to add some features.  

All commits and merge requests should follow [these conventions](https://www.conventionalcommits.org/).