extends Control

var layer_data

var exported_hotkey_map = {}

var hotkey_map = {}

var commands_map = {}

var selected_key

var layer_path

func _on_Button_pressed():
	var final_command = []
	for action in $VBoxContainer/Control/VBoxContainer/ScrollContainer/Actions.get_children():
		final_command.append(action.get_action_config())
#		final_command += action.get_action_command() + " && "
#	final_command.erase(final_command.length() - 2, 2)
#	print(final_command)
	hotkey_map[selected_key] = final_command
	print(hotkey_map)

func save_keymap():
	pass

func init(layer_data, layer_path):
	connect_keymap_buttons(get_node("VBoxContainer/ScrollContainer/KeyMap"))
	hotkey_map = layer_data
	self.layer_path = layer_path
	yield(self, "ready")
	_on_key_pressed($VBoxContainer/ScrollContainer/KeyMap/VBoxContainer3/VBoxContainer/HBoxContainer5/KEY_SPACE)

func connect_keymap_buttons(parent_node): # Recursively scan buttons to connect them to a function
	for child in parent_node.get_children():
		if child is Button:
			child.connect("pressed",self,"_on_key_pressed",[child])
		else:
			connect_keymap_buttons(child)

func _on_key_pressed(key_button):
	$VBoxContainer/Control/VBoxContainer/Label.text = "Keycode : " + key_button.name
	selected_key = key_button.name
	delete_children($VBoxContainer/Control/VBoxContainer/ScrollContainer/Actions)
	if not selected_key in hotkey_map:
		print('pas de clé !')
		_add_action()
	else:
		for action in hotkey_map[selected_key]:
			var t = _add_action()
			t.set_options(action)

func _add_action():
	var action_scene_instance = load("res://scenes/ActionLayout.tscn").instance()
	$VBoxContainer/Control/VBoxContainer/ScrollContainer/Actions.add_child(action_scene_instance)
	return action_scene_instance

func testtmp():
	for key in hotkey_map.keys():
		commands_map[key] = Plugins.from_configuration_to_command(hotkey_map[key])
	print(commands_map)
	_export_layer(commands_map)

func _export_layer(data):
	var file = File.new()
	if file.open(layer_path, File.WRITE) != 0:
		print("Error opening layer file !")
		return
	else:
		file.store_line(JSON.print(data))
		file.close()

func _save_layer():
	_on_Button_pressed()
	var file = File.new()
	var layer_save_file_path = layer_path
	layer_save_file_path.erase(layer_save_file_path.length() - 5, 5)
	if file.open(layer_save_file_path+".kbi", File.WRITE) != 0:
		print("Error opening layer file !")
		return
	else:
		file.store_line(JSON.print(hotkey_map))
		file.close()

func delete_children(node):
	for n in node.get_children():
		node.remove_child(n)
		n.queue_free()
