extends Node

func _ready():
	load_plugins()

var plugins = {}

func get_plugins_files_name():
	var files = []
	var dir = Directory.new()
	dir.open("plugins/")
	dir.list_dir_begin(true)

	var file_name = dir.get_next()
	while file_name != '':
		files += [file_name]
		file_name = dir.get_next()
	
	return files

func load_plugins():
	var files = get_plugins_files_name()
	for file_name in files:
		if ".json" in file_name:
			var file = File.new()
			file.open("plugins/"+file_name, file.READ)
			var text_json = file.get_as_text()
			var result_json = JSON.parse(text_json)
			var result = {}
			if result_json.error == OK:  # If parse OK
				var data = result_json.result
				plugins[data["title"]] = data
			else:  # If parse has errors
				print("JSON Error: ", result_json.error)
				print("JSON Error Line: ", result_json.error_line)
				print("JSON Error String: ", result_json.error_string)
				print("Couldn't parse plugin file !")


func add_plugin_user_input(user_input_container, method, arguments):
	match method:
		"LineEdit":
			var new_child = LineEdit.new()
			new_child.set_h_size_flags(3)
			new_child.placeholder_text = arguments["placeholder_text"]
			user_input_container.add_child(new_child)

func get_plugin_user_input_configuration(user_input_container,plugin_name,plugin_option):
	var result = {
		"plugin": plugin_name,
		"option": plugin_option,
		"parameters": []
	}
	for node in user_input_container.get_children():
		match node.get_class():
			"LineEdit":
				result["parameters"].append({"type": "LineEdit", "text":node.text})
	return result

func get_plugin_user_input(user_input_configuration):
	var result = []
	for input in user_input_configuration:
		match input["type"]:
			"LineEdit":
				result.append(input["text"])
	return result

func fill_user_input(user_input_container, input_configuration):
	for node in user_input_container.get_children():
		for input in input_configuration:
			match node.get_class():
				"LineEdit":
					node.text = input["text"]

func from_configuration_to_command(configurations):
	var final_command = ""
	for action in configurations:
		var action_command = get_command_from_config(action["plugin"],action["option"],action["parameters"])
		if action_command != null and (action["plugin"] != "System" and action["option"] != "Change layer"): #dirty, will change
			final_command += (action_command + " & ")
		else:
			final_command += action_command
	return final_command

func get_command_from_config(plugin,option,parameters):
	var option_button = $OptionButton2
	var plugin_user_input = []
	if option != "No selection":
		if plugins[plugin]["options"][option].has('parameters'):
			plugin_user_input = get_plugin_user_input(parameters)
		var command = plugins[plugin]["options"][option]['command'].format(plugin_user_input)
		command = command.format(plugins[plugin]["settings"])
		if plugins[plugin].has('base_command'):
			command = plugins[plugin]['base_command'] + " " + command
		return command
	else:
		print('No Option selected for an action !')


