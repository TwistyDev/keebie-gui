extends HBoxContainer

var current_plugin = 0

func _ready():
	load_plugins()

func load_plugins():
	for plugin in Plugins.plugins.values():
		$OptionButton.add_item(plugin["title"])
	_switch_selected_plugin(0)
	setup_execute_method()

func setup_execute_method():
	$ExecuteMethodOptionButton.add_item("Execute separately")
	$ExecuteMethodOptionButton.add_item("Execute sequentially")

func _switch_selected_plugin(index):
	var option_button = $OptionButton2
	for item_id in range(option_button.get_item_count()):
		option_button.remove_item(0)
	option_button.add_item('No selection')
	for option in Plugins.plugins.values()[index]["options"].keys():
		print(option)
		option_button.add_item(option)
	option_button.set_item_text(0,'No selection')
	current_plugin = index

func _on_OptionButton2_item_selected(index):
	var option_button = $OptionButton2
	delete_children($PluginUserInput)
	if Plugins.plugins.values()[current_plugin]["options"][option_button.get_item_text(index)].has('parameters'):
		for input in Plugins.plugins.values()[current_plugin]["options"][option_button.get_item_text(index)]["parameters"]:
			Plugins.add_plugin_user_input($PluginUserInput, input["type"], input["arguments"])

func delete_children(node):
	for n in node.get_children():
		node.remove_child(n)
		n.queue_free()

func get_action_config():
	var option_button = $OptionButton2
	return Plugins.get_plugin_user_input_configuration($PluginUserInput,Plugins.plugins.keys()[current_plugin], option_button.get_item_text(option_button.selected))

func set_options(parameters):
	current_plugin = Plugins.plugins.keys().find(parameters['plugin'])
	$OptionButton.select(current_plugin)
	_switch_selected_plugin(current_plugin)
	var option_index = Plugins.plugins[parameters['plugin']]["options"].keys().find(parameters['option'])+1
	print(option_index)
	$OptionButton2.select(option_index)
	_on_OptionButton2_item_selected(option_index)
	Plugins.fill_user_input($PluginUserInput,parameters['parameters'])
	

func remove_action():
	get_parent().remove_child(self)
