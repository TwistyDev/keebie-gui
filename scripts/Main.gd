extends Control

var window_label = "Keebie Gui"

var keebie_path = "Keebie"

var dict = {}

# Called when the node enters the scene tree for the first time.
func _ready():
	change_window_title("Settings")
	load_profiles(keebie_path)

func change_window_title(current_profile):
	OS.set_window_title(window_label + ' - ' + current_profile)

func get_layer_files_name(path):
	var files = []
	var dir = Directory.new()
	dir.open(path+"/layers")
	dir.list_dir_begin(true)

	var file_name = dir.get_next()
	while file_name != '':
		files += [file_name]
		file_name = dir.get_next()
	
	return files

func load_profiles(path):
	var files = get_layer_files_name(path)
	
	for file_name in files:
		if ".kbi" in file_name:
			var file = File.new()
			file.open(path+"/layers/"+file_name, file.READ)
			var text_json = file.get_as_text()
			var result_json = JSON.parse(text_json)
			var result = {}
			if result_json.error == OK:  # If parse OK
				var data = result_json.result
				create_new_layer(file_name.left(file_name.length()-4),data)
			else:  # If parse has errors
				print("JSON Error: ", result_json.error)
				print("JSON Error Line: ", result_json.error_line)
				print("JSON Error String: ", result_json.error_string)
				print("Couldn't open layer file !")

func select_keebie_path():
	$KeebiePathFileDialog.popup()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_KeebiePathFileDialog_dir_selected(dir):
	print(dir)
	$"TabContainer/Global Settings/VBoxContainer/HBoxContainer/Label2".text = dir


func _save_all_layers():
	for layer in $TabContainer.get_children().slice(1, $TabContainer.get_child_count()):
		layer._save_layer()

func _export_all_layers():
	for layer in $TabContainer.get_children().slice(1, $TabContainer.get_child_count()):
		layer.testtmp()

func create_new_layer(layer_name,data={}):
	var layer_scene = load("res://scenes/Layer.tscn").instance()
	layer_scene.name = layer_name
	layer_scene.init(data,keebie_path+"/layers/"+layer_name+".json")
	$TabContainer.add_child(layer_scene)

func _on_NewLayerButton_pressed():
	create_new_layer($"TabContainer/Global Settings/VBoxContainer/HBoxContainer4/LayerNameLineEdit".text)

func _on_TabContainer_tab_changed(tab):
	change_window_title($TabContainer.get_children()[tab].name)
